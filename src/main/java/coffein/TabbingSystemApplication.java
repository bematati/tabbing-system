package coffein;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TabbingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(TabbingSystemApplication.class, args);
	}
}
