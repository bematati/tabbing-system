package coffein.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import coffein.model.Customer;
import coffein.model.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
  
  @Query
  List<Transaction> findByCustomerOrderByDate(Customer customer);
  
  Transaction findByCustomerAndDate(Customer customer, LocalDate date);
  
  List<Transaction>findAllByOrderByCustomer();
  
  Transaction findOneById(long transactionId);

}
