package coffein.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import coffein.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
  
  Page<Customer> findAllByOrderByFullName(Pageable pageable);

  @Query
  Page<Customer> findByFullNameContainingOrEmailContainingIgnoreCaseOrderByFullName(String fullNamePart, String emailPart, Pageable page);
  
  Customer findOneById(long id);
  
  Customer findOneByFullName(String fullName);
  
  Customer findOneByEmail(String email);
  
  boolean existsByFullName(String fullName);
  boolean existsByEmail(String email);
  
  // for testing
  List<Customer> findByFullNameIgnoreCaseContainingOrderByFullName(String fullNamePart);
}
