package coffein.repository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import coffein.model.Customer;
import coffein.model.Customer.CustomerType;
import coffein.model.Transaction;

@Component
public class TestDataPopulator {
  
  private static final List<String> TEST_CUSTOMER_NAMES = Arrays.asList("Brian Chippendale", "Jon Theodore", "Chad Smith",
      "Dave Grohl", "Buddy Rich", "Ringo Starr", "Keith Moon", "John Bonham", "Greg Saunier");
  
  @Autowired
  private CustomerRepository customerRepository;
  
  @Autowired
  private TransactionRepository transactionRepository;
  
  
  @PostConstruct
  private void createTestData() {
    if (customerRepository.count() == 0) {
    TEST_CUSTOMER_NAMES.stream().forEach(fullName -> createTestCustomer(fullName, 
        fullName.split(" ")[0].toLowerCase() + "@testmail.com"));
    }
  }
  
  @Transactional
  private Customer createTestCustomer(String fullName, String email) {
    
    Customer customer = new Customer(fullName, email);
    
      if(fullName.split(" ")[0].length() <5){
        customer.setType(CustomerType.STAFF_GO);
      } else if(fullName.split(" ")[0].length() == 5) {
        customer.setType(CustomerType.CASUAL);
      } else if(fullName.split(" ")[0].length() > 5) {
        customer.setType(CustomerType.ONE_WEEK);
      }
      
      customer = customerRepository.save(customer);
      
      Double balance = createTestTransactionList(customer);
      customer.setBalance(balance);
      customer.setBilled(balance);
   
    return customerRepository.save(customer);
  }
  
  
  @Transactional
  private double createTestTransactionList(Customer customer) {

    double balance = 0;
    LocalDate today = LocalDate.now();
    LocalDate startDate = today.minusDays(10);
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    LocalDate endDate = LocalDate.parse("30.04.2018", formatter);
    
    long futureDaysNumber = ChronoUnit.DAYS.between(today,  endDate);
    
    //list of the past 10 dates
    List<LocalDate> pastDaysRange = Stream.iterate(startDate, date -> date.plusDays(1)).limit(10).collect(Collectors.toList());
    
    // create transactions with random bills for the past 10 days
    for (LocalDate day : pastDaysRange) {
      int rand = new Random().nextInt(50);
      Transaction transaction = new Transaction(customer, day, rand);
      transaction.setDailyBalance(rand);
      transactionRepository.save(transaction);
      balance += rand;
    }
    
    // list of dates starting with today's (inclusive)
    List<LocalDate> futureDaysRange = Stream.iterate(today, date -> date.plusDays(1))
        .limit(futureDaysNumber)
        .collect(Collectors.toList());
    
    // create transactions with 0 value for future dates
    for (LocalDate day : futureDaysRange) {
      Transaction transaction = new Transaction(customer, day);
      transactionRepository.save(transaction);
    }
    
    return balance;
  }
}
