package coffein.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import coffein.model.Customer;
import coffein.model.Transaction;
import coffein.service.TransactionService;

@Controller
@RequestMapping(value="/edit-transaction.html")
public class CustomerTransactionController {
  
  @Autowired
  private TransactionService transactionService;
  
  @Autowired
  private CustomerDetailsController customerDetailsController;
  
  private Logger logger = LoggerFactory.getLogger(getClass());
  
  
  @RequestMapping(method=RequestMethod.GET)
  public String displayTransaction(Model model, 
      @RequestParam(required=true, name="transactionId") Long transactionId) {
    
    LocalDate today = LocalDate.now();
    
    Transaction editedTransaction = transactionService.getEditedTransaction(transactionId);
    
    model.addAttribute("editedTransaction", editedTransaction);
    Customer customer = editedTransaction.getCustomer();
    model.addAttribute("customer", customer);
    //customerRepository.save(customer);
    
    List<Transaction> transactionList = editedTransaction.getCustomer().getTransactionList();
    List<Transaction> preDateTransactionList = new ArrayList<Transaction>();
    List<Transaction> postDateTransactionList = new ArrayList<Transaction>();
    
    for (Transaction transaction : transactionList) {
      if(transaction.getDate().isBefore(editedTransaction.getDate())) {
        preDateTransactionList.add(transaction);
      }
      else if((transaction.getDate().isAfter(editedTransaction.getDate()))  && (transaction.getDate().isBefore(today.plusDays(1)))) {
        postDateTransactionList.add(transaction);
      }
    }
    
    model.addAttribute("preDateTransactionList", preDateTransactionList);
    model.addAttribute("postDateTransactionList", postDateTransactionList);
    
    logger.debug(editedTransaction.toString());
    
    return "edit-transaction";
   
  }
  
  
  @RequestMapping(method=RequestMethod.POST)
  public String modifyTransaction(Model model, @ModelAttribute("editedTransaction") Transaction editedTransaction) {
    
    double updatedPrice = editedTransaction.getPrice();
    double updatedInput = editedTransaction.getInput();
    String updatedNotes = editedTransaction.getNotes();
    
    if (updatedPrice < 0 || updatedInput < 0) {
      boolean amountError = true;
      model.addAttribute("amountError", amountError);
      return "edit-transaction";
    }
    
    transactionService.updateTransaction(editedTransaction.getId(), editedTransaction.getCustomer().getId(),
        updatedPrice, updatedInput, updatedNotes);

    logger.debug(editedTransaction.toString());
    
    return customerDetailsController.displayCustomer(model, editedTransaction.getCustomer().getId());
  }

}
