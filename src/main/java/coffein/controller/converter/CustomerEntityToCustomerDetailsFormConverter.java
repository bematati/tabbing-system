package coffein.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import coffein.controller.form.CustomerDetailsForm;
import coffein.model.Customer;

@Component
public class CustomerEntityToCustomerDetailsFormConverter implements Converter<Customer, CustomerDetailsForm> {
  
  // converts a Customer object to a CustomerDetailsForm object
  @Override
  public CustomerDetailsForm convert(Customer source) {
    
    CustomerDetailsForm form = new CustomerDetailsForm();
    form.setCustomer(source);
    
    return form;
  }

}
