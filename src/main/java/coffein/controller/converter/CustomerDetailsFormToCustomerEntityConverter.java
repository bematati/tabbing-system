package coffein.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import coffein.controller.form.CustomerDetailsForm;
import coffein.model.Customer;

@Component
public class CustomerDetailsFormToCustomerEntityConverter implements Converter<CustomerDetailsForm, Customer> {
  
  
  // converts a CustomerDetailsForm object to a Customer object (gets the Customer object from the form)
  @Override
  public Customer convert(CustomerDetailsForm source) {
    return source.getCustomer();
  }
  

}
