package coffein.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import coffein.service.CustomerService;

/**
 * This controller is responsible for displaying the list of customers that are in the DB
 * - all of them OR those who match the given search criteria.
 */
@Controller
public class CustomerListController {
  
  @Autowired
  private CustomerService customerService;
  
  LocalDate date = LocalDate.now();
  
  /**
   * Displays the list of all customers
   * @param model
   * @param page
   * @return
   */
  @RequestMapping(value = "/")
  public String displayCustomerList(Model model, Pageable page) {
        
    model.addAttribute("allCustomerList", customerService.getCustomerList(page));
    model.addAttribute("date", date);
    return "customer-list";
  }
    
  
  /**
   * Displays the list of the customers whose names match the (optional) searchExpression
   * @param model
   * @param searchExpression
   * @param page
   * @return
   */
  @RequestMapping(value = "/searchresults")
  public String displayCustomerSearchList(Model model, @RequestParam(required=false) String searchExpression, Pageable page) {
        
    model.addAttribute("searchResultsPage", customerService.searchByFullNameEmailContains(searchExpression, page));
    return "customer-searchresults";
  }
  
}
