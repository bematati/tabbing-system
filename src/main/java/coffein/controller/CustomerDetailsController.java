package coffein.controller;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import coffein.controller.form.CustomerDetailsForm;
import coffein.model.Customer;
import coffein.model.Transaction;
import coffein.service.CustomerService;
import coffein.service.MailService;
import coffein.service.TransactionService;
import coffein.service.exceptions.BalanceNotNullException;

/**
 * This controller is responsible for displaying individual customer data and
 * the possibility to add amount to; or subtract amount from the total payable
 * 'balance'; cleaning a tab; or deleting a customer.
 */
@Controller
@RequestMapping(value = "/customer-details.html")
public class CustomerDetailsController {

  private static final String CUSTOMERFORM = "customerForm";
  private static final String VIEW_CUSTOMER_DETAILS = "customer-details";

  private Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private CustomerService customerService;
  
  @Autowired
  private TransactionService transactionService;
  
  @Autowired
  private MailService mailService;

  @Resource
  private ConversionService conversionService;
  

  /**
   * displays the customer's details form with list of transactions if Id is selected or new customer
   * empty form if no Id selected
   * 
   * @param model
   * @param customerId
   * @return
   */
  @RequestMapping(method = RequestMethod.GET)
  public String displayCustomer(Model model, @RequestParam(required = false) Long customerId) {

    boolean messaging = false;
    
    model.addAttribute("date", LocalDate.now());
     
     if (customerId != null) {
       Customer customer = customerService.getCustomerDetails(customerId);
       
       CustomerDetailsForm form = conversionService.convert(customer, CustomerDetailsForm.class);
       model.addAttribute(CUSTOMERFORM, form);
       
       List<Transaction> editableTransactionList = transactionService.getEditableTransactionList(customer, LocalDate.now());
       model.addAttribute("editableTransactionList", editableTransactionList);
     } else {
         Customer customer = new Customer();
         CustomerDetailsForm form = conversionService.convert(customer, CustomerDetailsForm.class);
         model.addAttribute(CUSTOMERFORM, form);
     }
      return VIEW_CUSTOMER_DETAILS;
  }

  
  /**
   * Creates or updates Customer entity according to the model data received via
   * the http request
   */
  @RequestMapping(method = RequestMethod.POST)
  public String createOrUpdateCustomer(Model model,
      @Valid @ModelAttribute(CUSTOMERFORM) CustomerDetailsForm customerForm,
      BindingResult bindingResult) {
    
   //model.addAttribute("customerTransactionList", customerForm.getCustomer().getTransactionList());

    LocalDate date = LocalDate.now();
    
    if (customerForm.getAction() != null) {
      
      switch (customerForm.getAction()) {
      
      case SAVE:

        // if a binding error happened send back the user the form for
        // correction
        if (bindingResult.hasErrors()) {
          logger.error("binding error");
          boolean bindingError = true;
          model.addAttribute("bindingError", bindingError);
          return VIEW_CUSTOMER_DETAILS;
        }

        // display a warning if new/modified customer name or email already exists in the DB
        String inputName = customerForm.getCustomer().getFullName();
        String inputEmail = customerForm.getCustomer().getEmail();
        Long customerId = customerForm.getCustomer().getId();
        
        boolean customerExists = customerService.checkUserInputBeforeSave(customerForm.isModifiable(), 
            inputName, inputEmail, customerId);
        
        if (customerExists) {
          model.addAttribute("customerExists", customerExists);
          return VIEW_CUSTOMER_DETAILS;
        }
        
        // SAVE
        Customer customer = conversionService.convert(customerForm, Customer.class);
        Customer savedCustomer = customerService.saveCustomer(customer);
        
        logger.info("Customer {} has been saved", savedCustomer.getFullName());
        logger.debug(customer.toString());
        
        if (customerForm.isModifiable()) {
        logger.info("Customer {} has been modified", savedCustomer.getFullName());
        customerForm.setModifiable(false);
        }
        else {
          // adding default transactions to new customer
          List<Transaction> defaultTransactionList = transactionService.createDefaultTransactionList(savedCustomer);
          savedCustomer.setTransactionList(defaultTransactionList);
        }
        
        return VIEW_CUSTOMER_DETAILS;
      
      case DELETE_CUSTOMER:
        
        try {
          customerService.deleteCustomer(customerForm.getCustomer().getId());
          logger.info("Customer {} has been deleted", customerForm.getCustomer().getFullName());
        } catch (BalanceNotNullException e) {
          System.out.println(e.toString());
          boolean hasDebt = true;
          model.addAttribute("hasDebt", hasDebt);

          return VIEW_CUSTOMER_DETAILS;
        }
        
        break;

      
      case MODIFY:
        
        customerForm.setModifiable(true);
        logger.debug(customerForm.getCustomer().getFullName() + " is being modified");
        return VIEW_CUSTOMER_DETAILS;

      
      case DISPLAY_TEMPLATE_MAIL:
        
        boolean messaging = true;
        model.addAttribute("messaging", messaging);
        logger.debug("Email template is added for " + customerForm.getCustomer().getFullName());

        String templateEmail = "Hello, " + customerForm.getCustomer().getFullName() + ",\n\n"
            + "Just a friendly reminder about your tab at Kahu Café.\n\n"
            + "Your tab is $" + customerForm.getCustomer().getBalance() + " as of today (" + date + ")."
            + "\n\n";

        customerForm.setMessage(templateEmail);

        return VIEW_CUSTOMER_DETAILS;

      
      case SENDMAIL:
        
        String message = customerForm.getMessage();

        mailService.sendMailMessage(customerForm.getCustomer().getEmail(), message);
        return displayCustomer(model, customerForm.getCustomer().getId());
        
        
      case CANCEL: //Unused
        
        if (customerForm.isModifiable()) {
          customerForm.setModifiable(false);
        }
        // returns the unsaved customer
        return VIEW_CUSTOMER_DETAILS;

      
      case BACK:
        
        break;

      }
    }
    return "redirect:/";

  }
}
