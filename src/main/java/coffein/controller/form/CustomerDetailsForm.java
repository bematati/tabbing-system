package coffein.controller.form;

import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.ControllerAdvice;

import coffein.model.Customer;

@ControllerAdvice
public class CustomerDetailsForm {

  @Valid
  @NotNull
  private Customer customer;

  LocalDate localDate = LocalDate.now();

  private String message = "email body";
  
  private boolean modifiable;
  
  @NotNull
  private Action action;

  public static enum Action {
    DELETE_CUSTOMER, SAVE, CANCEL, BACK, MODIFY, SENDMAIL, DISPLAY_TEMPLATE_MAIL
  }

  public Customer getCustomer() {
    return customer;
  }
  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }
  
  public Action getAction() {
    return action;
  }
  public void setAction(Action action) {
    this.action = action;
  }

  public LocalDate getLocalDate() {
    return localDate;
  }
  public void setLocalDate(LocalDate localDate) {
    this.localDate = localDate;
  }

  public boolean isModifiable() {
    return modifiable;
  }
  public void setModifiable(boolean modifiable) {
    this.modifiable = modifiable;
  }
  

}
