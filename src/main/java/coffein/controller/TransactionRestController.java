package coffein.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import coffein.model.Customer;
import coffein.model.Transaction;
import coffein.repository.CustomerRepository;
import coffein.repository.TransactionRepository;

@RestController
public class TransactionRestController {

  private Logger logger = LoggerFactory.getLogger(getClass());
  
  @Autowired
  private TransactionRepository transactionRepository;
  
  @Autowired
  private CustomerRepository customerRepository;
  
  @RequestMapping(value="/transactions", method=RequestMethod.GET, produces="application/json")
  public @ResponseBody List<Transaction> populateTransactionList(@RequestParam("customerId") Long customerId) {
    
    Customer customer = customerRepository.findOneById(customerId);
    
    logger.debug("restcontroller on");
    
    List<Transaction> transactionList = customer.getTransactionList();
    logger.debug(transactionList.get(0).toString());
    
    return transactionList;
  }
  
//  
//  @GetMapping(value="/all")
//  public Response addTransaction(@RequestBody Transaction transaction, @ModelAttribute List<Transaction> transactionList) {
//    
//    transactionList.add(transaction);
//    transactionRepository.save(transaction);
//
//    //    transactionService.updateTransaction(transaction.getId(), transaction.getCustomer().getId(),
//    //        transaction.getPrice(), transaction.getInput(), transaction.getNotes());
//    
//    //create response object
//    Response response = new Response("Done", transactionList);
//    logger.debug("REST POST got");
//    return response;
//  }
}
