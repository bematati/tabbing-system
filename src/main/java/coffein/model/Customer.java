package coffein.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Validated
@Entity
@Table(name = "Customers", indexes = { @Index(name = "CUSTOMER_IDX", columnList = "fullName,email") })
public class Customer {

  public Customer() {
  }

  public Customer(String fullName, String email) {
    this.fullName = fullName;
    this.email = email;
  }

  private Long id;
  
  private Long version;

  @Size(min = 3, max = 80)
  @NotNull
  private String fullName;

  @NotNull
  private CustomerType type;

  @Size(max = 80)
  private String email;

  private String phone;
  private double balance;

  private double billed;
  private double paid;

  private List<Transaction> transactionList;

  public static enum CustomerType {
    STAFF_GO, CASUAL, ONE_WEEK
  }

  public CustomerType getType() {
    return type;
  }

  public void setType(CustomerType type) {
    this.type = type;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
  @Version
  public Long getVersion() {
    return version;
  }

  protected void setVersion(Long version) {
    this.version = version;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  public double getBilled() {
    return billed;
  }

  public void setBilled(double billed) {
    this.billed = billed;
  }

  public double getPaid() {
    return paid;
  }

  public void setPaid(double paid) {
    this.paid = paid;
  }

  @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
  @JsonManagedReference
  public List<Transaction> getTransactionList() {
    return transactionList;
  }

  public void setTransactionList(List<Transaction> transactionList) {
    this.transactionList = transactionList;
  }

  @Override
  public String toString() {
    return "\nCustomer [Name: " + fullName + ", Email: " + email + ", \n" + "Balance: " + balance+ " ]";
  }
}
