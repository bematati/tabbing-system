package coffein.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Table(name = "Transactions", indexes = { @Index(name = "TRANS_IDX", columnList = "date") })
public class Transaction implements Serializable {

  
  private static final long serialVersionUID = -654812L;

  private Long id;
  
  private Long version;
  
  private double price;
  private double input;
  private double dailyBalance;
  private String notes;

  private Customer customer;

  LocalDate date;

  public Transaction() {
  }

  public Transaction(Customer customer, LocalDate date) {
    this.customer = customer;
    this.date = date;
  }

  public Transaction(Customer customer, LocalDate date, double price) {
    this.customer = customer;
    this.date = date;
    this.price = price;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getInput() {
    return input;
  }

  public void setInput(double input) {
    this.input = input;
  }

  @JsonSerialize(using=LocalDateSerializer.class)
  @JsonDeserialize(using = LocalDateDeserializer.class)
  @JsonFormat
  (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy ")
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public double getDailyBalance() {
    return dailyBalance;
  }

  public void setDailyBalance(double dailyBalance) {
    this.dailyBalance = dailyBalance;
  }

  @ManyToOne
  @JoinColumn(name = "customer_id")
  @JsonBackReference
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }
  
  
  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  @Override
  public String toString() {
    return "Transaction [id: " + id + ", date: " + date + ", bill: " +
  + price + ", input: " + input + ", costumer_id: " + customer.getId() + "]" ;
  }

}
