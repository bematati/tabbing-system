package coffein.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import coffein.model.Customer;
import coffein.service.exceptions.BalanceNotNullException;

public interface CustomerService {

  /**
   * Returns the list of all customers
   * 
   * @param page
   * @return
   */
  Page<Customer> getCustomerList(Pageable page);

  /**
   * Returns the list of customers filtered by the search expression
   *
   */
  Page<Customer> searchByFullNameEmailContains(String searchExpression, Pageable page);

  /**
   * Displays the selected Customer's page
   * 
   * @param customerId
   * @return Customer object
   */
  Customer getCustomerDetails(long customerId);

  Customer saveCustomer(Customer customer);
  
  void deleteCustomer(Long customerId) throws BalanceNotNullException;
  
  public boolean checkUserInputBeforeSave(boolean modifiable, String newCustomerName, String newCustomerEmail,
      Long customerId);

  Customer updateAmount(Long customerId, double newBill, double newPaid);
  

}
