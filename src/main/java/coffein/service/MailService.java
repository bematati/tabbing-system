package coffein.service;

public interface MailService {
  
  void sendMailMessage(String to, String message);
  
}
