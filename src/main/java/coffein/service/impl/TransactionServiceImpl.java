package coffein.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import coffein.model.Customer;
import coffein.model.Transaction;
import coffein.repository.TransactionRepository;
import coffein.service.CustomerService;
import coffein.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {
  
  @Autowired
  private TransactionRepository transactionRepository;
  
  @Autowired
  private CustomerService customerService;
  
  private Logger logger = LoggerFactory.getLogger(getClass());
  
  
  @Transactional
  public List<Transaction> getEditableTransactionList(Customer customer, LocalDate date) {
    
    Transaction actualTransaction = transactionRepository.findByCustomerAndDate(customer, date);
    
    List<Transaction> editableTransactionList = new ArrayList<Transaction>();
    List<Transaction> transactionList = customer.getTransactionList();
    
    for (Transaction transaction : transactionList) {
      if(transaction.getDate().isBefore(actualTransaction.getDate()) 
          || transaction.getDate().isEqual(actualTransaction.getDate())) {
        editableTransactionList.add(transaction);
      }
    }
    return editableTransactionList;
  }
  
  
//  public Transaction getActualTransaction(Customer customer, LocalDate date) {
//    
//    Transaction actualTransaction = null;
//    try {
//      logger.debug(customer.toString());
//      actualTransaction = transactionRepository.findByCustomerAndDate(customer, date);
//      logger.debug("transaction found: " + actualTransaction.toString());
//    } catch (Exception e) {
//      logger.error(e.toString());
//    }
//    
//    return actualTransaction;
//  }
  
  @Transactional
  public List<Transaction> createDefaultTransactionList(Customer customer) {
    
    List<Transaction> defaultTransactionList = new ArrayList<>();
    
    
    LocalDate startDate = LocalDate.now();
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    LocalDate endDate = LocalDate.parse("30.04.2018", formatter);
    
    long days = ChronoUnit.DAYS.between(startDate,  endDate);
          
    
    List<LocalDate> daysRange = Stream.iterate(startDate, date -> date.plusDays(1))
        .limit(days)
        .collect(Collectors.toList());
    
    for (LocalDate day : daysRange) {
      Transaction transaction = new Transaction(customer, day);
      Transaction savedTransaction = transactionRepository.save(transaction);
      defaultTransactionList.add(savedTransaction);
      
    }
    
    return defaultTransactionList;
    
  }
  
  public void updateTransaction(Long transactionId, Long customerId, double price, double input, String notes) {
    
    Transaction editedTransaction = transactionRepository.findOneById(transactionId);
    
    double newBilled = price - editedTransaction.getPrice();
    double newPaid = input - editedTransaction.getInput();
    
    logger.debug(editedTransaction.toString());
    editedTransaction.setPrice(price);
    editedTransaction.setInput(input);
    editedTransaction.setNotes(notes);
    editedTransaction.setDailyBalance(price - input);
    
    customerService.updateAmount(editedTransaction.getCustomer().getId(), newBilled, newPaid);
    
    transactionRepository.save(editedTransaction);
    
  }


  @Override
  public Transaction getEditedTransaction(Long transactionId) {
    Transaction editedTransaction = transactionRepository.findOneById(transactionId);
    return editedTransaction;
  }


}
