package coffein.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import coffein.model.Customer;
import coffein.model.Customer.CustomerType;
import coffein.repository.CustomerRepository;
import coffein.service.CustomerService;
import coffein.service.TransactionService;
import coffein.service.exceptions.BalanceNotNullException;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  TransactionService transactionService;
  
  
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Transactional(readOnly = true)
  public Page<Customer> getCustomerList(Pageable page) {
    Page<Customer> customerList = null;
    customerList = customerRepository.findAllByOrderByFullName(page);
    return customerList;
  }

  @Transactional(readOnly = true)
  public Customer getCustomerDetails(long customerId) {
    Customer selectedCustomer = customerRepository.findOneById(customerId);
    
    CustomerType type = selectedCustomer.getType();
    return selectedCustomer;
  }

  @Override
  public Page<Customer> searchByFullNameEmailContains(String searchExpression, Pageable page) {

    Page<Customer> customerResult = null;

    if (searchExpression == null || searchExpression.length() == 0) {
      logger.debug("Performing full seach");
      customerResult = customerRepository.findAllByOrderByFullName(page);
    } else {
      searchExpression = searchExpression.trim();

      customerResult = customerRepository
          .findByFullNameContainingOrEmailContainingIgnoreCaseOrderByFullName(searchExpression, searchExpression, page);
    }
    return customerResult;
  }


  @Transactional
  public Customer updateAmount(Long customerId, double newBill, double newPaid) {
    Customer customer = customerRepository.findOneById(customerId);

    customer.setBilled(customer.getBilled() + newBill);
    customer.setPaid(customer.getPaid() + newPaid);
    customer.setBalance(customer.getBalance() + newBill - newPaid);
    
    customerRepository.save(customer);
    
    return customer;
  }
  

  @Transactional
  public Customer saveCustomer(Customer customer) {

    return customerRepository.save(customer);
  }

  @Transactional
  public void deleteCustomer(Long customerId) throws BalanceNotNullException {

    if (customerRepository.findOneById(customerId).getBalance() != 0) {
      throw new BalanceNotNullException();
    }

    customerRepository.delete(customerRepository.findOneById(customerId));
  }

  @Override
  public boolean checkUserInputBeforeSave(boolean modifiable, String newCustomerName, String newCustomerEmail,
      Long customerId) {
    
    boolean customerExists = false;
    
    if (customerId == null)  {
      if (customerRepository.existsByFullName(newCustomerName)
          || customerRepository.existsByEmail(newCustomerEmail)) {
        customerExists = true;
      }
    }
      
    if (modifiable) {
      if ((customerRepository.existsByFullName(newCustomerName)
          && customerRepository.findOneByFullName(newCustomerName).getId() != customerId)
          || (customerRepository.existsByEmail(newCustomerEmail) 
          && customerRepository.findOneByEmail(newCustomerEmail).getId() != customerId)) {
          customerExists = true;
      }
    }
    return customerExists;
  } 
  
}
