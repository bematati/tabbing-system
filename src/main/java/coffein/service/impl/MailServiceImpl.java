package coffein.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import coffein.service.MailService;

@Service
public class MailServiceImpl implements MailService {
  
  @Autowired
  public JavaMailSender emailSender;
  
  @Autowired
  private TemplateEngine templateEngine;
 

  @Transactional
  public void sendMailMessage(String to, String message) {
    try{
      MimeMessage mimeMessage = emailSender.createMimeMessage();
      MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
      messageHelper.setFrom("robertdanyi@gmail.com");
      messageHelper.setTo(to);
      messageHelper.setSubject("Message from Kahu");
      String content = build(message);  // = templateEngine.process("email-template", context)
      messageHelper.setText(content, true);
      
      emailSender.send(mimeMessage);
      
    } catch (MailException e) {
      e.printStackTrace();
    } catch (MessagingException e) {
      e.printStackTrace();
    }
  }
  
  public String build(String message) {
    Context context = new Context();
    context.setVariable("message", message);
    return templateEngine.process("email-template", context);
  }

}
