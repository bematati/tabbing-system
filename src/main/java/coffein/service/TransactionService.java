package coffein.service;

import java.time.LocalDate;
import java.util.List;

import coffein.model.Customer;
import coffein.model.Transaction;

public interface TransactionService {
  
  List<Transaction> getEditableTransactionList(Customer customer, LocalDate date);
  
  // public Transaction getActualTransaction(Customer customer, LocalDate date);
  
  public Transaction getEditedTransaction(Long transactionId);
  
  public void updateTransaction(Long transactionId, Long customerId, double updatedPrice, double updatedInput, String updatedNotes);
  
  public List<Transaction> createDefaultTransactionList(Customer customer);

}
