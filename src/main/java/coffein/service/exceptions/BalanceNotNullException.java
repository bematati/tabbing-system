package coffein.service.exceptions;

public class BalanceNotNullException extends Exception {
  
  public String toString() {
    return "Error: Customer's balance is not null, customer cannot be deleted.";
  }

}
