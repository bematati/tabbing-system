package org.training;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import coffein.controller.form.CustomerDetailsForm;
import coffein.model.Customer;
import coffein.repository.CustomerRepository;

@SpringBootTest
@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryTest {
  
  private Logger logger = LoggerFactory.getLogger(getClass());
  
  @Autowired
  private CustomerRepository customerRepository;
  
  private Customer createTestCustomer(String fullName, String email) {
    
    Customer customer = new Customer(fullName, email); 
        
    return customerRepository.save(customer);
    
    
  }
  
  @Before
  public void createTestData() {
    Arrays.asList("Javier Rodriguez Rodriguez", "John Wick", "Bill Gates", "Steve Jobs", "Nicola Tesla")
    .stream()
    .forEach(name -> createTestCustomer(name, name.toLowerCase().split(" ")[0] + "@testmail.com"));
  }
  
  @Test
  public void testFindCustomer() {
    
    Iterable<Customer> allCustomers = customerRepository.findAll();
    allCustomers.forEach(customer -> logger.debug("Customer: {}", customer));
    
    long firstCustomerId = allCustomers.iterator().next().getId();
    
    Customer customer = customerRepository.findOneById(firstCustomerId);
    
    assertThat(customer).isNotNull();
    
    logger.debug("Customer: {}", customer);
    
    assertThat(customer.getFullName()).isEqualTo("Javier Rodriguez Rodriguez");
    assertThat(customer.getEmail()).isEqualTo("javier@testmail.com");
     
  }
  
  @Test
  public void testFindByPartOfName() {
    
    List<Customer> customersFound = customerRepository.findByFullNameIgnoreCaseContainingOrderByFullName("jo");
    
    assertThat(customersFound).hasSize(2);
    assertThat(customersFound.get(0).getFullName()).isEqualTo("John Wick");
    assertThat(customersFound.get(1).getFullName()).isEqualTo("Steve Jobs");
    }
  
  @Test
  public void testCreateCustomer() {
    
    CustomerDetailsForm customerForm = new CustomerDetailsForm();
    
    customerForm.setCustomer(new Customer("Seth Green", "seth@testnewmail.com"));
    
    customerRepository.save(customerForm.getCustomer());
    
    assertThat(customerRepository.findOneByFullName("Seth Green").getId()).isNotNull();
    
    logger.debug("Test Customer {} with email address {} has been created.", customerRepository.findOneByFullName("Seth Green").getFullName(),
        customerRepository.findOneByFullName("Seth Green").getEmail());
 
  }
  
  @Test
  public void testModifyCustomer() {
    
    Customer customer = customerRepository.findOneByFullName("Bill Gates");
    long customerId = customer.getId();
    
    customer.setFullName("Daniel Day-Lewis");
    customer.setEmail("daniel@newmail.com");
    
    customerRepository.save(customer);
    
    logger.debug("Customer data have been modified, checking...");
    
    Customer checkCustomer = customerRepository.findOneById(customerId);
    assertThat(checkCustomer.getFullName()).isEqualTo("Daniel Day-Lewis");
    assertThat(checkCustomer.getEmail()).isEqualTo("daniel@newmail.com");
  }
}
